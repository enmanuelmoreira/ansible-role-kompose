# Ansible Role: Kompose

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-kompose/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-kompose/-/commits/main)

This role installs [Kompose](https://github.com/kubernetes/kompose) binary on any supported host.

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    kompose_version: latest # v1.26.0 if you want a specific version
    kompose_package_name: kompose
    kompose_repo_path: https://github.com/kubernetes/kompose/releases/download
    kompose_bin_path: /usr/local/bin/kompose


This role can install the latest or a specific version. See [available Kompose releases](https://github.com/kubernetes/kompose) and change this variable accordingly.

    kompose_version: latest # v1.26.0 if you want a specific version

The path to the Kompose home repository.

    kompose_repo_path: https://github.com/kubernetes/kompose/releases/download

The name of the kompose package (Debian and RedHat distro families)

    kompose_package_name: kompose

The location where the hugo binary will be installed.

    kompose_bin_path: /usr/local/bin/kompose

Kompose supports amd64, arm and arm8 CPU architectures. You can adjust the CPU architecture where the binary will be installed.

    kompose_arch: amd64 # amd64, arm, arm64

Kompose needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install Kompose. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: kompose

## License

MIT / BSD
